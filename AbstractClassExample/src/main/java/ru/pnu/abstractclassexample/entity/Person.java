/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.pnu.abstractclassexample.entity;

/**
 *
 * @author developer
 */
public class Person {
    
    private String id;              // Уникальный номер
    
    private String firstName;       // Имя
    private String sureName;        // Фамилия     
    private String middleName;      // Отчество  

    
    
    public Person(String id, String sureName, String firstName,String middleName) {
        this.id = id;
        this.firstName = firstName;
        this.sureName = sureName;
        this.middleName = middleName;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Персона: {");
        sb.append("id=").append(id);
        sb.append(", firstName=").append(firstName);
        sb.append(", sureName=").append(sureName);
        sb.append(", middleName=").append(middleName);
        sb.append('}');
        return sb.toString();
    }
    
    

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSureName() {
        return sureName;
    }

    public void setSureName(String sureName) {
        this.sureName = sureName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
    
    
    
    
    
}
