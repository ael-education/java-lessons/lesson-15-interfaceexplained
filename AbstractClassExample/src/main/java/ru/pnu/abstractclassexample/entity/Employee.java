/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.pnu.abstractclassexample.entity;

/**
 *
 * @author developer
 */
public class Employee extends Person {
 
    
    private String position;        // Должность

    public Employee(String id, String sureName, String firstName, String middleName, String position) {
        super(id, sureName, firstName, middleName);
        this.position = position;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Cотрудник {");
        sb.append("position=").append(position);
        sb.append(",").append(super.toString());
        sb.append('}');
        return sb.toString();
    }
    
    
    
    
}
