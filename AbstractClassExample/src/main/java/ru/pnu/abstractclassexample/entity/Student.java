/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.pnu.abstractclassexample.entity;

/**
 *
 * @author developer
 */
public class Student extends Person {

    
    private String studentGroup;    // Группа
    private String recordBook;      // Группа
    private int studyYear;          // Год обучения (курс)
    
    /**
     * Конструктор из родительского класса Person
     *
     * @param id
     * @param firstName
     * @param sureName
     * @param middleName
     */
    public Student(String id, String sureName, String firstName, String middleName) {
        super(id, sureName, firstName, middleName);
    }

    public Student(String id, String sureName, String firstName, String middleName, String studentGroup, String recordBook, int studyYear) {
        super(id, sureName, firstName, middleName);
        this.studentGroup = studentGroup;
        this.recordBook = recordBook;
        this.studyYear = studyYear;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Студент {");
        sb.append("studentGroup=").append(studentGroup);
        sb.append(", recordBook=").append(recordBook);
        sb.append(", studyYear=").append(studyYear);
        sb.append(",").append(super.toString());
        sb.append('}');
        return sb.toString();
    }

    
    

    public String getStudentGroup() {
        return studentGroup;
    }

    public void setStudentGroup(String studentGroup) {
        this.studentGroup = studentGroup;
    }

    public String getRecordBook() {
        return recordBook;
    }

    public void setRecordBook(String recordBook) {
        this.recordBook = recordBook;
    }

    public int getStudyYear() {
        return studyYear;
    }

    public void setStudyYear(int studyYear) {
        this.studyYear = studyYear;
    }

}
