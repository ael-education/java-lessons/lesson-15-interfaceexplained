/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package ru.pnu.abstractclassexample;

import ru.pnu.abstractclassexample.entity.Employee;
import ru.pnu.abstractclassexample.entity.Student;

/**
 *
 * @author developer
 */
public class AbstractClassExample {

    public static void main(String[] args) {
        System.out.println("Изучение абстрактных классов");
        
        Student s1 = new Student("3", "Кузьминов", "Данила", "Андреевич", "Пи(Б)-21", "000003", 2);
        
        String position = "плотник";
        
        
        Employee e2 = new Employee(s1.getId(), s1.getSureName(), s1.getFirstName(), s1.getMiddleName(), "электрик");
        
        System.out.println(s1.toString());
        System.out.println(e2.toString());
      
        
        
        
    }
}
